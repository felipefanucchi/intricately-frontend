import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const types = {
  TAB_ITEMS_SUCCESS: 'buildAside',
  TAB_CHANGE_SUCCESS: 'setCurrentTab'
}

const store = new Vuex.Store({
  state: {
    tabItems: [],
    currentTab: ''
  },
  mutations: {
    [types.TAB_ITEMS_SUCCESS]: (state, payload) => state.tabItems = payload,
    [types.TAB_CHANGE_SUCCESS]: (state, payload) => state.currentTab = payload
  },
  getters: {
    getTabItems: state => state.tabItems
  },
  actions: {
    fetchTabItems: ({ commit }) => {
      const items = ['Company Data', 'Company Table', 'Company Page'];
      setTimeout(() => {
        commit(types.TAB_ITEMS_SUCCESS, items);
        commit(types.TAB_CHANGE_SUCCESS, items[0]);
      }, 300);
    }
  },
});

export { store };